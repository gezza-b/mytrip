GetNumberOfStationsIntent how many train stations are there
GetNumberOfStationsIntent how many stations are there
GetNumberOfStationsIntent how many train stations do you know
GetNumberOfStationsIntent how many train stations are in New South Wales
GetNumberOfStationsIntent how many train stations are there in New South Wales
GetNumberOfStationsIntent for the total number of train stations
GetNumberOfStationsIntent the total number of train stations
GetNumberOfStationsIntent the number of stations
GetNumberOfStationsIntent the number of train stations

GetNextTrainIntent for the next train
GetNextTrainIntent next train
GetNextTrainIntent what is the next train
GetNextTrainIntent when is my next train
GetNextTrainIntent when is our next train
GetNextTrainIntent when is the next train
GetNextTrainIntent when is my next train
GetNextTrainIntent when is our next train
GetNextTrainIntent when is the next train leaving
GetNextTrainIntent when is my next train leaving
GetNextTrainIntent when is our next train leaving
GetNextTrainIntent when does the next train depart
GetNextTrainIntent when does my next train depart
GetNextTrainIntent when does my next train leave
GetNextTrainIntent when does our next train leave
GetNextTrainIntent when does the train leave
GetNextTrainIntent what is the next train
